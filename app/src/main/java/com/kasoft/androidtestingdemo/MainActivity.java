package com.kasoft.androidtestingdemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kasoft.androidtestingdemo.utils.ValidationUtils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressDialog mProgressDialog;

    private EditText edtUsername, edtPassword;
    private Button btnLogin, btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtUsername = (EditText) findViewById(R.id.edt_user_name);

        btnLogin = (Button) findViewById(R.id.btn_login);
        btnSignUp = (Button) findViewById(R.id.btn_sign_up);

        btnLogin.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
    }

    private void performLogin(String username, String password) {
        mProgressDialog = ProgressDialog.show(MainActivity.this, null, "Login...");
        new LoginTask().execute();
    }

    private void navigateNextActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    private void validateLoginData() {
        String username = edtUsername.getText().toString();
        String password = edtPassword.getText().toString();

        if (!ValidationUtils.isValidUsername(username)) {
            edtUsername.requestFocus();
            edtUsername.setSelection(0, username.length());
            edtUsername.setError("Username is invalid");
        } else if (!ValidationUtils.isValidPassword(password)) {
            edtPassword.requestFocus();
            edtPassword.setText(null);
            edtPassword.setError("Password is invalid");
        } else {
            performLogin(username, password);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnSignUp) {

        } else if (v == btnLogin) {
            validateLoginData();
        }
    }


    private class LoginTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            mProgressDialog = ProgressDialog.show(MainActivity.this, null, "Login...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mProgressDialog.dismiss();
            navigateNextActivity();
        }
    }
}
