package com.kasoft.androidtestingdemo.utils;

import android.content.Context;
import android.support.test.espresso.IdlingResource;

import com.kasoft.androidtestingdemo.R;

/**
 * Created by Khanh Nguyen on 1/13/2017
 */

public class StringUtils implements IdlingResource {

    public static String getTabNameByIndex(Context context, int index) {
        switch (index) {
            case 0:
                return context.getString(R.string.tab_name_0);
            case 1:
                return context.getString(R.string.tab_name_1);
            case 2:
                return context.getString(R.string.tab_name_3);
            default:
                return null;
        }
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public boolean isIdleNow() {
        return false;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback callback) {

    }
}
