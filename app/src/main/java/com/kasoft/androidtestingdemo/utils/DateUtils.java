package com.kasoft.androidtestingdemo.utils;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Khanh Nguyen on 1/13/2017
 */

public class DateUtils {
    private static final String TIME_FORMAT = "hh:mm";

    public static String getFormattedTime(long time) {
        DateFormat dateFormat = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
        return dateFormat.format(new Date(time));
    }
}
