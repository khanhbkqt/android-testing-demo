package com.kasoft.androidtestingdemo.utils;

/**
 * Created by Khanh Nguyen on 1/13/2017
 */

public class ValidationUtils {

    public static boolean isValidPassword(String password) {
        return password.length() >= 8 && !password.toLowerCase().equals(password);
    }

    public static boolean isValidUsername(String username) {
        return username.length() >= 8 && !username.contains(" ");
    }
}
