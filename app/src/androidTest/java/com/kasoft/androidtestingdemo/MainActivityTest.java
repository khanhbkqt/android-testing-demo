package com.kasoft.androidtestingdemo;

import android.app.Activity;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collection;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.runner.lifecycle.Stage.RESUMED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Khanh Nguyen on 1/13/2017
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mMainActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void errorMessageIfEmptyUsernameAndPasswordTest() {
        onView(withId(R.id.edt_user_name)).perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.btn_login)).perform(click());

        onView(withId(R.id.edt_user_name)).check(matches(hasErrorText("Username is invalid")));
    }

    @Test
    public void errorMessageIfInvalidUsernameTest() {
        onView(withId(R.id.edt_user_name)).perform(typeText("user name"), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).perform(typeText("pAssword"), closeSoftKeyboard());
        onView(withId(R.id.btn_login)).perform(click());

        onView(withId(R.id.edt_user_name)).check(matches(hasErrorText("Username is invalid")));
    }

    @Test
    public void errorMessageEmptyPasswordTest() {
        onView(withId(R.id.edt_user_name)).perform(typeText("khanhbkqt"), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.btn_login)).perform(click());

        onView(withId(R.id.edt_password)).check(matches(hasErrorText("Password is invalid")));
    }

    @Test
    public void homeActivityIsDisplayedIfValidUsernameAndPasswordTest() {
        onView(withId(R.id.edt_user_name)).perform(typeText("khanhbkqt"), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).perform(typeText("123Abcde"), closeSoftKeyboard());
        onView(withId(R.id.btn_login)).perform(click());

        Activity currentActivity = getActivityInstance();
        assertNotNull(currentActivity);
        assertTrue(currentActivity instanceof HomeActivity);
    }

    public Activity getActivityInstance() {
        final Activity[] activity = new Activity[1];
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable( ) {
            public void run() {
                Activity currentActivity;
                Collection resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(RESUMED);
                if (resumedActivities.iterator().hasNext()){
                    currentActivity = (Activity) resumedActivities.iterator().next();
                    activity[0] = currentActivity;
                }
            }
        });

        return activity[0];
    }
}