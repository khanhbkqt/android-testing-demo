package com.kasoft.androidtestingdemo.utils;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.kasoft.androidtestingdemo.R;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Khanh Nguyen on 1/13/2017
 */
public class StringUtilsTest {

    private Context context;

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getTargetContext();
    }

    /**
     * Given index = 0
     *
     */
    @Test
    public void getTabNameByIndexTestCase1() throws Exception {
        String expected = context.getString(R.string.tab_name_0);
        String actual = StringUtils.getTabNameByIndex(context, 0);
        assertEquals(expected, actual);
    }

    /**
     * Given index = 1
     *
     */
    @Test
    public void getTabNameByIndexTestCase2() throws Exception {
        String expected = context.getString(R.string.tab_name_1);
        String actual = StringUtils.getTabNameByIndex(context, 1);
        assertEquals(expected, actual);
    }

    /**
     * Given index = 2
     *
     */
    @Test
    public void getTabNameByIndexTestCase3() throws Exception {
        String expected = context.getString(R.string.tab_name_2);
        String actual = StringUtils.getTabNameByIndex(context, 2);
        assertEquals(expected, actual);
    }

    /**
     * Given index = 3
     *
     */
    @Test
    public void getTabNameByIndexTestCase4() throws Exception {
        String actual = StringUtils.getTabNameByIndex(context, 4);
        assertNull(actual);
    }

}