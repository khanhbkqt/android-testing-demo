package com.kasoft.androidtestingdemo.utils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Khanh Nguyen on 1/13/2017
 */
public class DateUtilsTest {

    @Before
    public void setUp() {

    }


    /**
     * Scenario:    Test the getFormattedTime function
     * Given:       Time as long 1484291955901
     *
     */
    @Test
    public void testGetFormattedTime() {
        String expected = "02:19";
        String actual = DateUtils.getFormattedTime(1484291955901L);
        assertEquals(expected, actual);
    }
}